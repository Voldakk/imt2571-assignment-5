<?php
/**
 * Created by IntelliJ IDEA.
 * User: eivind
 * Date: 04.11.17
 * Time: 17:03
 */

include_once('View.php');


Class ImportView extends View {
    protected $fileName;
    protected $skierCount;
    protected $clubCount;
    protected $seasonCount;

    /** Constructor
     */
    public function __construct($skierCount, $clubCount, $seasonCount)
    {
        $this->fileName = $_GET['filename'];
        $this->skierCount = $skierCount;
        $this->clubCount = $clubCount;
        $this->seasonCount = $seasonCount;
    }

    /** Used by the superclass to generate page title
     * @return string Page title to be generated.
     */
    protected function getPageTitle() {
        return 'XML import';
    }

    /** Used by the superclass to generate page content
     * @return string Content of page to be generated.
     */
    protected function getPageContent() {
        $content = <<<HTML
<p>Imported file: $this->fileName </p>
<p>Skier count: $this->skierCount</p>
<p>Club count: $this->clubCount</p>
<p>Season count: $this->seasonCount</p>
HTML;
        return $content;
    }
}