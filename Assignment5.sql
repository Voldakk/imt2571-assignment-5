-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 05, 2017 at 06:45 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Assignment5`
--

-- --------------------------------------------------------

--
-- Table structure for table `City`
--

CREATE TABLE `City` (
  `Id` int(11) NOT NULL,
  `Name` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `CountyId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Club`
--

CREATE TABLE `Club` (
  `Id` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `Name` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `CityId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `County`
--

CREATE TABLE `County` (
  `Id` int(11) NOT NULL,
  `Name` varchar(250) COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Season`
--

CREATE TABLE `Season` (
  `SkierUserName` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `FallYear` year(4) NOT NULL,
  `ClubId` varchar(250) COLLATE utf8_danish_ci DEFAULT NULL,
  `TotalDistance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Skier`
--

CREATE TABLE `Skier` (
  `UserName` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `FirstName` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `LastName` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `YearOfBirth` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `City`
--
ALTER TABLE `City`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `CityCounty` (`CountyId`);

--
-- Indexes for table `Club`
--
ALTER TABLE `Club`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `ClubCity` (`CityId`);

--
-- Indexes for table `County`
--
ALTER TABLE `County`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Season`
--
ALTER TABLE `Season`
  ADD PRIMARY KEY (`SkierUserName`,`FallYear`),
  ADD KEY `SeasonClub` (`ClubId`);

--
-- Indexes for table `Skier`
--
ALTER TABLE `Skier`
  ADD PRIMARY KEY (`UserName`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `City`
--
ALTER TABLE `City`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `County`
--
ALTER TABLE `County`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `City`
--
ALTER TABLE `City`
  ADD CONSTRAINT `CityCounty` FOREIGN KEY (`CountyId`) REFERENCES `County` (`Id`);

--
-- Constraints for table `Club`
--
ALTER TABLE `Club`
  ADD CONSTRAINT `ClubCity` FOREIGN KEY (`CityId`) REFERENCES `City` (`Id`);

--
-- Constraints for table `Season`
--
ALTER TABLE `Season`
  ADD CONSTRAINT `SeasonClub` FOREIGN KEY (`ClubId`) REFERENCES `Club` (`Id`),
  ADD CONSTRAINT `SeasonSkier` FOREIGN KEY (`SkierUserName`) REFERENCES `Skier` (`UserName`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
