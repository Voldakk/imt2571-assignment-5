<?php
/**
 * Created by IntelliJ IDEA.
 * User: eivind
 * Date: 04.11.17
 * Time: 16:59
 */

include_once("Model/DBModel.php");
include_once("Model/XMLModel.php");
include_once("Model/Skier.php");
include_once("Model/Club.php");
include_once("Model/Season.php");
include_once("Model/Entry.php");
include_once("View/View.php");
include_once("View/ImportView.php");
include_once("View/ErrorView.php");

class Controller
{
    public $xmlModel;
    public $dbModel;
    public $test;

    public function __construct()
    {
        session_start();
        $this->xmlModel = new XMLModel();
        $this->dbModel = new DBModel();
    }

    public function invoke()
    {
        if (isset($_GET['filename'])) {
            $this->readXML();
            $this->exportToDB();

            $view = new ImportView($this->xmlModel->skierCount(), $this->xmlModel->clubCount(), $this->xmlModel->seasonCount());
            $view->create();
        }
    }

    public function readXML()
    {
        try {
            $doc = new DOMDocument();
            if (!$doc->load($_GET["filename"])) {
                // Handle parsing error
                $view = new ErrorView("Unable to parse file");
                $view->create();
            } else {
                $xpath = new DOMXpath($doc);

                // Skiers
                $skiers = $xpath->query("/SkierLogs/Skiers/Skier");
                foreach ($skiers as $skier) {
                    $userName = $skier->getAttribute("userName");
                    $firstName = $skier->getElementsByTagName("FirstName")[0]->nodeValue;
                    $lastName = $skier->getElementsByTagName("LastName")[0]->nodeValue;
                    $yearOfBirth = $skier->getElementsByTagName("YearOfBirth")[0]->nodeValue;

                    $this->xmlModel->addSkier(new Skier($userName, $firstName, $lastName, $yearOfBirth));
                }

                // Clubs
                $clubs = $xpath->query("/SkierLogs/Clubs/Club");
                foreach ($clubs as $club) {
                    $id = $club->getAttribute("id");
                    $name = $club->getElementsByTagName("Name")[0]->nodeValue;
                    $city = $club->getElementsByTagName("City")[0]->nodeValue;
                    $county = $club->getElementsByTagName("County")[0]->nodeValue;

                    // County and city
                    $county = $this->xmlModel->addCounty($county);
                    $city = $this->xmlModel->addCity($city, $county);

                    // Add the club
                    $this->xmlModel->addClub(new Club($id, $name, $city));
                }

                // Seasons
                $seasons = $xpath->query("/SkierLogs/Season");
                foreach ($seasons as $season) {
                    $fallYear = $season->getAttribute("fallYear");

                    $skiers = $this->xmlModel->getSkiers();
                    foreach ($skiers as $skier) {
                        $clubs = $this->xmlModel->getClubs();
                        foreach ($clubs as $club) {
                            $log = array();
                            $entries = $xpath->query('/SkierLogs/Season[@fallYear=' . $fallYear . ']/Skiers[@clubId="' . $club->id . '"]/Skier[@userName="' . $skier->userName . '"]/Log/Entry');
                            foreach ($entries as $entry) {
                                $date = $entry->getElementsByTagName("Date")[0]->nodeValue;
                                $area = $entry->getElementsByTagName("Area")[0]->nodeValue;
                                $distance = $entry->getElementsByTagName("Distance")[0]->nodeValue;

                                $log[count($log)] = new Entry($date, $area, $distance);
                            }
                            if (count($log) > 0) {
                                $this->xmlModel->addSeason(new Season($skier->userName, $club->id, $fallYear, $log));
                            }
                        }

                        // No club
                        $log = array();
                        $entries = $xpath->query('/SkierLogs/Season[@fallYear=' . $fallYear . ']/Skiers[not(@clubId)]/Skier[@userName="' . $skier->userName . '"]/Log/Entry');
                        foreach ($entries as $entry) {
                            $date = $entry->getElementsByTagName("Date")[0]->nodeValue;
                            $area = $entry->getElementsByTagName("Area")[0]->nodeValue;
                            $distance = $entry->getElementsByTagName("Distance")[0]->nodeValue;

                            $log[count($log)] = new Entry($date, $area, $distance);
                        }
                        if (count($log) > 0) {
                            $this->xmlModel->addSeason(new Season($skier->userName, null, $fallYear, $log));
                        }
                    }

                }
            }
        } catch (Exception $e) {
            $view = new ErrorView($e->getMessage());
            $view->create();
            return;
        }
    }

    public function exportToDB()
    {
        // Add counties
        $counties = $this->xmlModel->getCounties();
        foreach ($counties as $county) {
            $this->dbModel->addCounty($county);
        }

        // Add cities
        $cities = $this->xmlModel->getCities();
        foreach ($cities as $city) {
            $this->dbModel->addCity($city);
        }

        // Add skiers
        $skiers = $this->xmlModel->getSkiers();
        foreach ($skiers as $skier) {
            $this->dbModel->addSkier($skier);
        }

        // Add clubs
        $clubs = $this->xmlModel->getClubs();
        foreach ($clubs as $club) {
            $this->dbModel->addClub($club);
        }

        // Add seasons
        $seasons = $this->xmlModel->getSeasons();
        foreach ($seasons as $season) {
            $this->dbModel->addSeason($season);
        }
    }
}