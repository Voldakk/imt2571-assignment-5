<?php
/**
 * Created by IntelliJ IDEA.
 * User: eivind
 * Date: 04.11.17
 * Time: 20:02
 */

include_once("Model/City.php");
include_once("Model/County.php");

class XMLModel
{
    private $skiers;
    private $clubs;
    private $seasons;

    private $counties;
    private $cities;

    public function __construct()
    {
        $this->skiers = array();
        $this->clubs = array();
        $this->seasons = array();

        $this->counties = array();
        $this->cities = array();
    }

    public function addSkier($skier)
    {
        $this->skiers[count($this->skiers)] = $skier;
    }

    public function skierCount()
    {
        return count($this->skiers);
    }

    public function getSkiers()
    {
        return $this->skiers;
    }

    public function addClub($club)
    {
        $this->clubs[count($this->clubs)] = $club;
    }

    public function clubCount()
    {
        return count($this->clubs);
    }

    public function getClubs()
    {
        return $this->clubs;
    }

    public function addSeason($season)
    {
        $this->seasons[count($this->seasons)] = $season;
    }

    public function seasonCount()
    {
        return count($this->seasons);
    }

    public function getSeasons()
    {
        return $this->seasons;
    }

    public function addCounty($county)
    {
        if(array_key_exists($county, $this->counties))
            return $this->counties[$county];
        else {
            $this->counties[$county] = new County($county);
            return $this->counties[$county];
        }
    }

    public function getCounties()
    {
        return array_values($this->counties);
    }

    public function addCity($city, $county)
    {
        if(array_key_exists($city, $this->cities))
            return $this->cities[$city.$county->name];
        else {
            $this->cities[$city.$county->name] = new City($city, $county);
            return $this->cities[$city.$county->name];
        }
    }

    public function getCities()
    {
        return array_values($this->cities);
    }
}