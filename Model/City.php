<?php
/**
 * Created by IntelliJ IDEA.
 * User: eivind
 * Date: 04.11.17
 * Time: 22:38
 */

include_once("Model/County.php");

class City
{
    public $id;
    public $name;
    public $county;

    /** Constructor
     * @param string $name The city name
     * @param County $county The city county
     */
    public function __construct($name, $county)
    {
        $this->name = $name;
        $this->county = $county;
    }
}