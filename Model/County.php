<?php
/**
 * Created by IntelliJ IDEA.
 * User: eivind
 * Date: 05.11.17
 * Time: 18:21
 */

class County
{
    public $id;
    public $name;

    /** Constructor
     * @param string $name The county name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }
}