<?php
/**
 * Created by IntelliJ IDEA.
 * User: eivind
 * Date: 04.11.17
 * Time: 20:06
 */

class Entry
{
    public $date;
    public $area;
    public $distance;

    /** Constructor
     * @param int $date The entry date
     * @param string $area The area
     * @param int $distance The distance
     */
    public function __construct($date, $area, $distance)
    {
        $this->date = $date;
        $this->area = $area;
        $this->distance = $distance;
    }
}