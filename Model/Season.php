<?php
/**
 * Created by IntelliJ IDEA.
 * User: eivind
 * Date: 04.11.17
 * Time: 19:56
 */

class Season
{
    public $username;
    public $clubId;
    public $fallYear;
    public $log;

    /** Constructor
     * @param string $username The skier username
     * @param string $clubId The club id
     * @param int $fallYear The year
     * @param array $log The list of log entries
     */
    public function __construct($username, $clubId, $fallYear, $log)
    {
        $this->username = $username;
        $this->clubId = $clubId;
        $this->fallYear = $fallYear;
        $this->log = $log;
    }

    public function getTotalDistance()
    {
        if($this->log == null)
            return 0;

        $totalDistance = 0;
        foreach ($this->log as $entry)
        {
            $totalDistance += $entry->distance;
        }

        return $totalDistance;
    }
}