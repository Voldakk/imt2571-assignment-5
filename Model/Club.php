<?php
/**
 * Created by IntelliJ IDEA.
 * User: eivind
 * Date: 04.11.17
 * Time: 17:05
 */

include_once("Model/City.php");

class Club
{
    public $id;
    public $name;
    public $city;

    /** Constructor
     * @param string $id The club id
     * @param string $name The club name
     * @param City $city The club city
     */
    public function __construct($id, $name, $city)
    {
        $this->id = $id;
        $this->name = $name;
        $this->city = $city;
    }
}