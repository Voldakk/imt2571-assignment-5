<?php
/**
 * Created by IntelliJ IDEA.
 * User: eivind
 * Date: 04.11.17
 * Time: 17:03
 */

class DBModel
{
    protected $db = null;

    /**
     * @throws PDOException
     */
    public function __construct($db = null)
    {
        if ($db)
        {
            $this->db = $db;
        }
        else
        {
            $this->db = new PDO('mysql:host=localhost;dbname=Assignment5;charset=utf8mb4', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
    }

    /** Adds a new county to the collection.
     * @param $county County The county to be added
     * @throws PDOException
     * @throws InvalidArgumentException
     */
    public function addCounty($county)
    {
        if($county == '')
            throw new InvalidArgumentException('Invalid input');

        $stmt = $this->db->prepare('INSERT INTO County(Name) VALUES(:Name)');
        $stmt->bindValue(':Name', $county->name);
        $stmt->execute();
        $county->id = $this->db->lastInsertId();
    }

/*
    public function getCountyId($countyName)
    {
        if($countyName == '')
            throw new InvalidArgumentException('Invalid input');

        $stmt = $this->db->prepare('SELECT Id FROM County WHERE Name=:Name');
        $stmt->bindValue(':Name', $countyName);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if($row)
        {
            return $row['Id'];
        }
        return null;
    }*/

    /** Adds a new city to the collection.
     * @param $city City The city to be added
     * @throws PDOException
     * @throws InvalidArgumentException
     */
    public function addCity(City $city)
    {
        if($city == '')
            throw new InvalidArgumentException('Invalid input');

        $stmt = $this->db->prepare('INSERT INTO City(Name, CountyId) VALUES(:Name, :CountyId)');
        $stmt->bindValue(':Name', $city->name);
        $stmt->bindValue(':CountyId', $city->county->id);
        $stmt->execute();
        $city->id = $this->db->lastInsertId();
    }
/*
    public function getCityId($cityName, $countyName)
    {
        if($countyName == '')
            throw new InvalidArgumentException('Invalid input');

        $stmt = $this->db->prepare('SELECT Id FROM County WHERE Name=:Name');
        $stmt->bindValue(':Name', $countyName);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if($row)
        {
            return $row['Id'];
        }
        return null;
    }*/

    /** Adds a new skier to the collection.
     * @param $skier Skier The skier to be added
     * @throws PDOException
     * @throws InvalidArgumentException
     */
    public function addSkier(Skier $skier)
    {
        if($skier->userName == '' || $skier->firstName == ''|| $skier->lastName == ''|| $skier->yearOfBirth == 0)
            throw new InvalidArgumentException('Invalid input');

        $stmt = $this->db->prepare('INSERT INTO Skier(UserName, FirstName, LastName, YearOfBirth) VALUES(:UserName, :FirstName, :LastName, :YearOfBirth)');
        $stmt->bindValue(':UserName', $skier->userName);
        $stmt->bindValue(':FirstName', $skier->firstName);
        $stmt->bindValue(':LastName', $skier->lastName);
        $stmt->bindValue(':YearOfBirth', $skier->yearOfBirth);
        $stmt->execute();
    }

    /** Adds a new club to the collection.
     * @param $club Club The club to be added
     * @throws PDOException
     * @throws InvalidArgumentException
     */
    public function addClub(Club $club)
    {
        if($club->id == '' || $club->name == ''|| $club->city == null)
            throw new InvalidArgumentException('Invalid input');

        $stmt = $this->db->prepare('INSERT INTO Club(Id, Name, CityId) VALUES(:Id, :Name, :CityId)');
        $stmt->bindValue(':Id', $club->id);
        $stmt->bindValue(':Name', $club->name);
        $stmt->bindValue(':CityId', $club->city->id);
        $stmt->execute();
    }

    /** Adds a new season to the collection.
     * @param $season Season The season to be added
     * @throws PDOException
     * @throws InvalidArgumentException
     */
    public function addSeason(Season $season)
    {
        if($season->username == '' || $season->fallYear == null)
            throw new InvalidArgumentException('Invalid input');

        $stmt = $this->db->prepare('INSERT INTO Season(SkierUserName, FallYear, ClubId, TotalDistance) VALUES(:SkierUserName, :FallYear, :ClubId, :TotalDistance)');
        $stmt->bindValue(':SkierUserName', $season->username);
        $stmt->bindValue(':FallYear', $season->fallYear);
        $stmt->bindValue(':ClubId', $season->clubId);
        $stmt->bindValue(':TotalDistance', $season->getTotalDistance());
        $stmt->execute();
    }
}