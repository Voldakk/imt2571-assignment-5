<?php
/**
 * Created by IntelliJ IDEA.
 * User: eivind
 * Date: 04.11.17
 * Time: 17:09
 */

class Skier {
    public $userName;
    public $firstName;
    public $lastName;
    public $yearOfBirth;

    /** Constructor
     * @param string $userName The skiers username
     * @param string $firstName The skiers first name
     * @param string $lastName The skiers last name
     * @param int $yearOfBirth The skiers year of birth
     */
    public function __construct($userName, $firstName, $lastName, $yearOfBirth)
    {
        $this->userName = $userName;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->yearOfBirth = $yearOfBirth;
    }
}